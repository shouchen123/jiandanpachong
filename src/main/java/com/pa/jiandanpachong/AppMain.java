package com.pa.jiandanpachong;  

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class AppMain implements PageProcessor {

    //列表页正则表达式
    private static final String REGEX_PAGE_URL = "http://md\\.itlun\\.cn/a/new/list_11_\\w+\\.html";

    // 爬取的列表页，页数。
    private static int PAGE_SIZE = 10;
    
    private static String IMG_PATH = "E:\\image\\";

    //配置
    private Site site = Site.me();

    //爬取
    public void process(Page page) {
        /**
         * 把所有的列表页都添加到 爬取的目标URL中
         */
        List<String> targetRequests = new ArrayList<String>();
        for (int i = 1;i < PAGE_SIZE; i++){
            targetRequests.add("http://md.itlun.cn/a/new/list_11_"+i+".html");
        }
        page.addTargetRequests(targetRequests);
        //用正则匹配是否是列表页
        if (page.getUrl().regex(REGEX_PAGE_URL).match()) {
            /**
             * 如果是，获取 class为 lady-name 的a 标签 的所有链接(详情页)。
             */
//        	List<String> urls = page.getHtml().xpath("//a[@class=\"lady-name\"]").links().all();
            List<String> urls = page.getHtml().xpath("//UL//script").all();
            for (String url:urls) {
//            	System.out.println(url.indexOf("http"));
//            	System.out.println(url.lastIndexOf("\";"));
//            	System.out.println(url.substring(url.indexOf("http"),url.lastIndexOf("\";")));
            	url = url.substring(url.indexOf("http"),url.lastIndexOf("\";"));
            	 try {
					DownloadImage.download(url,IMG_PATH+url.substring(url.indexOf("-")-1, url.length()));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        } else {
            // 如果不是,则
            // 获取 class为mm-p-info-cell clearfix 的ul /li /span/文本，作为图片保存图片名。
           // String nickname = page.getHtml().xpath("//ul[@class=\"mm-p-info-cell clearfix\"]/li/span/text()").toString();
            // 获取 class为mm-p-modelCard 的div /a /img  src 值，此为图片URL.
            String imgUrl = page.getHtml().xpath("//img").css("img","src").toString();
            System.out.println(imgUrl);
            try {
                // 根据图片URL 下载图片方法
                /**
                 * String 图片URL地址
                 * String 图片名称
                 * String 保存路径
                 */
                DownloadImage.download("http:" + imgUrl+ ".jpg","E:\\image\\");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        //启动
    	Scanner scanner = new Scanner(System.in);
    	System.out.println("请输入图片保存路径");
    	IMG_PATH = scanner.next();
    	System.out.println("请输入爬取页数");
    	PAGE_SIZE = scanner.nextInt();
    	System.out.println("请输入开启线程数");
    	int i = scanner.nextInt();
        Spider.create(new AppMain())
                //添加初始化的URL
                .addUrl("http://md.itlun.cn/a/new/list_11_1.html")
                //启动10个线程
                .thread(i)
                //运行
                .run();
    }

}