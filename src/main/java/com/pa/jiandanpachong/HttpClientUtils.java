package com.pa.jiandanpachong;
import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClientUtils {
	
	public static void sendHttpGet(String urlStr) throws Exception{
		HttpURLConnection urlCon = null;
		try{
			//System.out.println(urlStr);
			URL url = new URL("http://blog.csdn.net/wgyscsf/article/details/52741464");
			urlCon = (HttpURLConnection)url.openConnection(); 
			urlCon.setRequestMethod("GET");
			urlCon.setRequestProperty("Content-type", "text/html");
			urlCon.setRequestProperty("Accept-Charset", "utf-8");
			urlCon.setRequestProperty("contentType", "utf-8");
			urlCon.setConnectTimeout(10000);
			urlCon.setReadTimeout(10000); 
			urlCon.connect();
		    DataInputStream dis = new DataInputStream(urlCon.getInputStream());  
	        if (urlCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
	           throw new Exception("网络错误异常！!!!");
	       }
	        dis.close();
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("发送清理drug缓存出错,请检查系统配置!");
		}finally{
			if(urlCon != null){
				urlCon.disconnect();
			}
		}
	}

}
